import {
  BaseModel,
  Attribute,
  JsonApiModelConfig
} from '../base.model';

@JsonApiModelConfig({
  type: 'campaignSettings'
})
export class CampaignSettings extends BaseModel {
  @Attribute() leaveVoicemail: number;
  @Attribute() approvedCallers:	boolean;
  @Attribute() scrub: boolean;

  campaignId?: number;
}

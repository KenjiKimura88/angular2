import { BaseModel, Attribute, JsonApiModelConfig } from '../base.model';

@JsonApiModelConfig({
  type: 'addresses'
})
export class Address extends BaseModel {
  @Attribute()
  recipient: string;

  @Attribute()
  line1: string;

  @Attribute()
  line2: string;

  @Attribute()
  city: string;

  @Attribute()
  postalCode: number;

  @Attribute()
  region: string;

  @Attribute()
  country: string;

  contactId?: string;
}

import { BaseModel, Attribute, JsonApiModelConfig, BelongsTo } from '../base.model';
import {Campaign} from './campaign';

@JsonApiModelConfig({
  type: 'followUpActions'
})
export class FollowUpActions extends BaseModel {
  @BelongsTo()
  campaign: Campaign;

  @Attribute()
  email: number;

  @Attribute()
  callBack: number;

  @Attribute()
  call_back: number; // API limitation, GET returns call_back, PATCH saves callBack

  @Attribute()
  appointment: number;

  @Attribute()
  campaignId: number;

  @Attribute()
  campaign_id: number;
}

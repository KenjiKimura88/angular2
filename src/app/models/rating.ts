import { BaseModel, Attribute, JsonApiModelConfig } from '../base.model';

@JsonApiModelConfig({
  type: 'ratings'
})
export class Rating extends BaseModel {
  @Attribute() rate: number;
  @Attribute() review: string;
  @Attribute() tip: string;

  campaignCallerId?: number;
  callId?: string;
}

import { HasMany, BelongsTo } from 'angular2-jsonapi';
import {
  BaseModel,
  Attribute,
  JsonApiModelConfig
} from '../base.model';
import { CampaignCall } from './campaign-call';
import { Phone } from './phone';
import { Caller } from './caller';
import { Address } from './address';
import { SocialLinks } from './social-links';
import { KeyValues } from './key-values';
import { FollowUpResponses } from './follow-up-responses';
import { QuestionResponse } from './question-response';

export type CampaignContactStatuses = 'dnc'
  | 'failed'
  | 'incompleted'
  | 'completed'
  | 'deleted'
  | 'voicemail';

@JsonApiModelConfig({
  type: 'contacts'
})

export class CampaignContact extends BaseModel {
  @Attribute()
  callAt: string;

  @Attribute()
  campaignId: string;

  @Attribute()
  email: string;

  @Attribute()
  firstName: string;

  @Attribute()
  lastName: string;

  get fullName(): string {
    return `${this.firstName || ''} ${this.lastName || ''}`.trim();
  }

  @Attribute()
  info: string;

  @Attribute()
  title: string;

  @Attribute()
  company: string;

  @Attribute()
  hints: string;

  @Attribute()
  status: any;

  set _status(set: CampaignContactStatuses) {
    this.__status = set;
    this.triedCall = ['voicemail', 'completed', 'failed'].indexOf(this.__status) !== -1;
    this.cssColor = ({
      completed: 'green',
      voicemail: 'orange',
      incompleted: 'green',
      dnc: 'red',
      failed: 'red',
      untried: 'gray',
    })[set];
  }

  get _status(): CampaignContactStatuses {
    return this.__status;
  }

  @HasMany()
  get calls(): CampaignCall[] {
    return this._campaignCall;
  }

  set calls(set: CampaignCall[]) {
    this._campaignCall = set;

    if (!set || !set.length) {
      this.lastCall = null;
      return;
    }
    let lastCall = set[set.length - 1];
    this.lastCall = lastCall;
    if (lastCall.caller) {
      this.lastCaller = lastCall.caller;
    }
  }

  @BelongsTo() address: Address;
  @BelongsTo() phone: Phone;
  @BelongsTo() socialLink: SocialLinks;
  @BelongsTo() followUpResponse: FollowUpResponses;
  @HasMany() keyvalues: KeyValues[];
  @HasMany() questionResponses: QuestionResponse[];

  triedCall: boolean;
  lastCall: CampaignCall;
  lastCaller: Caller;
  cssColor: string;
  private _campaignCall: CampaignCall[];
  private __status: CampaignContactStatuses;
}

import { BaseModel, Attribute, JsonApiModelConfig, BelongsTo } from '../base.model';
import { Address } from './address';
import { Picture } from './picture';
import { Phone } from './phone';
import { SocialLinks } from './social-links';

@JsonApiModelConfig({
  type: 'companies'
})
export class Company extends BaseModel {
  @Attribute() name: string;
  @Attribute() about: string;
  @Attribute() industryId: string;
  @Attribute() officialName: string;

  @BelongsTo() socialLink: SocialLinks;
  @BelongsTo() phone: Phone;
  @BelongsTo() picture: Picture;
  @BelongsTo() address: Address;
}

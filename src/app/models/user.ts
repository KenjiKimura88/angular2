import {
  BaseModel,
  Attribute,
  JsonApiModelConfig
} from '../base.model';

@JsonApiModelConfig({
  type: 'users'
})
export class User extends BaseModel {
  @Attribute()
  email: string;
  @Attribute()
  firstName: string;
  @Attribute()
  lastName: string;
  @Attribute()
  status: string;
  @Attribute()
  role: string;
  @Attribute()
  timezone: string;
  @Attribute()
  uid: string;

  get fullName(): string {
    return `${this.firstName || ''} ${this.lastName || ''}`.trim();
  }
}

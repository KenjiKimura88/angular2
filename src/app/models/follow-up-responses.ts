import {
  BaseModel,
  Attribute,
  JsonApiModelConfig
} from '../base.model';
@JsonApiModelConfig({
  type: 'followUpResponses'
})
export class FollowUpResponses extends BaseModel {

  @Attribute()
  email: number;

  @Attribute()
  callBack: number;

  @Attribute()
  appointment: number;

  @Attribute()
  contactId: number;

  @Attribute()
  createdAt: string;

  @Attribute()
  updatedAt: string;
}

import { BaseModel, Attribute, JsonApiModelConfig } from '../base.model';
import { BelongsTo, HasMany } from 'angular2-jsonapi';
import { Question } from './question';
import { QuestionField } from './question.field';
@JsonApiModelConfig({
  type: 'questionResponses'
})
export class QuestionResponse extends BaseModel {

  @Attribute()
  questionId: number;

  @Attribute()
  contactId: number;

  @Attribute()
  result: string;

  @Attribute()
  comment: string;

  @BelongsTo() question: Question;
  @HasMany() questionFields: QuestionField[];

}

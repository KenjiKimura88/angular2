import { HasMany } from 'angular2-jsonapi';
import { BaseModel, Attribute, JsonApiModelConfig } from '../base.model';
import { QuestionField } from './question.field';
import { QuestionResponse } from './question-response';

type ResponseType = 'multiple_checkbox'
  | 'free_text'
  | 'multiple_radio'
  | 'nps'
  | 'freeform'
  | 'calendar'
  | 'stars';

@JsonApiModelConfig({
  type: 'questions'
})
export class Question extends BaseModel {
  @Attribute()
  question: string;

  @Attribute()
  responseType: ResponseType;

  @Attribute()
  explanations: string;

  @Attribute()
  comment: boolean;

  @Attribute()
  campaignId: number;

  @Attribute()
  position: number;

  @HasMany() questionFields: QuestionField[];
  @HasMany() questionResponses: QuestionResponse[];
}

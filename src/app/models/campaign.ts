import { BaseModel, Attribute, HasMany, BelongsTo, JsonApiModelConfig } from '../base.model';
import { Question } from './question';
import { FollowUpActions } from './follow-up-actions';
import { CampaignSettings } from './campaign-settings';
import { CampaignBudget } from './campaign-budget';
import { CampaignCallerId } from './campaign-caller-id';
import { CallerId } from './caller-id';

export interface ICampaignCategory {
  label: string;
  description: string;
  types: Object;
}

export var CampaignTypesByCategory: Array<ICampaignCategory> = [
  {
    label: 'Sales',
    description: 'sales description',
    types: {
      lead_gen: 'Generate leads',
      inform_product: 'Increase product awareness',
      appt_setting: 'Set appointments'
    }
  },
  {
    label: 'Data collection',
    description: 'data collection description',
    types: {
      inquire_biz: 'Collect business information',
      verify_info: 'Verify some data'
    }
  },
  {
    label: 'Feedback & survey',
    description: 'feedback and survey description',
    types: {
      feedback: 'Collect client feedback',
      mystery_shopping: 'Mystery shopping',
      market_research: 'Market research',
      customer_support: 'Customer support'
    }
  },
  {
    label: 'Inform clients',
    description: 'inform clients description',
    types: {
      confirm_attendance: 'Confirm attendance',
      alert_users: 'Alert existing users'
    }
  },
  {
    label: 'Other',
    description: 'other description',
    types: {
      other: 'Other'
    }
  }
];

enum CampaignStatus {
  draft, pending, ready, pause, low_credit, completed, archived, run
}

@JsonApiModelConfig({
  type: 'campaigns'
})
export class Campaign extends BaseModel {
  @BelongsTo()
  followUpAction: FollowUpActions;

  @BelongsTo()
  campaignSetting: CampaignSettings;

  @HasMany()
  questions: Question[];

  @BelongsTo()
  campaignBudget: CampaignBudget;

  @BelongsTo()
  campaignCallerId: CampaignCallerId;

  @BelongsTo()
  callerId: CallerId;

  @Attribute()
  fromCallerId: string;

  @Attribute()
  name: string;

  @Attribute()
  createdAt: string;

  @Attribute()
  updatedAt: string;

  @Attribute()
  startDate: string;

  @Attribute()
  endDate: string;

  @Attribute()
  fromNumber: number;

  @Attribute()
  cost: number;

  @Attribute()
  language: string;

  @Attribute()
  status: CampaignStatus;

  @Attribute()
  progress: number;

  @Attribute()
  kind: string;

  @Attribute()
  alwaysOpen: boolean;

  @Attribute()
  instructions: string;

  @Attribute()
  extraInstructions: string;

  @Attribute()
  pitch: string;

  @Attribute()
  numberOfQuestions: number;

  @Attribute()
  numberOfContacts: number;

  getFollowUpAction(): FollowUpActions {
    return this.followUpAction;
  }

  getSettings(): CampaignSettings {
    return this.campaignSetting;
  }

  getQuestions(): Question[] {
    return this.questions;
  }
}

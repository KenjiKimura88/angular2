import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'replace'
})
export class ReplacePipe implements PipeTransform {
  transform(string: string, find: any, replacement: any): any {
    return string.replace(find, replacement);
  }
}

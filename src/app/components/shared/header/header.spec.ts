import {
  TestBed,
  async
} from '@angular/core/testing';

// Load the implementations that should be tested
import { HeaderComponent } from './header.component';

describe('Header', () => {
  let fixture;
  // provide our implementations or mocks to the dependency injector
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [HeaderComponent],
    });
  });


  it('should find header element', async(() => {
    TestBed.compileComponents().then(() => {
      fixture = TestBed.createComponent(HeaderComponent);
      fixture.detectChanges();
      let header = fixture.debugElement.query(By.css('header'));
      console.log('header.nativeElement', header.nativeElement);
      expect(header.nativeElement.classNames).toContain('header');
    });
  }));
});

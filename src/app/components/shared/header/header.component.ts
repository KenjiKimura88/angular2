import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth/auth.service';

@Component({
  selector: 'header-app',
  styleUrls: [],
  templateUrl: 'header.template.html'
})

export class HeaderComponent implements OnInit {
  get loggedUser(): any {
    return this._authService.currentUserData;
  };

  constructor(private _authService: AuthService) {
  }

  logOut() {
    this._authService.doLogout();
  }

  ngOnInit() {
  }
}

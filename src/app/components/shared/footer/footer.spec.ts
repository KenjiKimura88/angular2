import {
  ActivatedRoute,
  Data
} from '@angular/router';
import {
  inject,
  TestBed
} from '@angular/core/testing';

// Load the implementations that should be tested
import { FooterComponent } from './footer.component';

describe('footer', () => {
  // provide our implementations or mocks to the dependency injector
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      // provide a better mock
      {
        provide: ActivatedRoute,
        useValue: {
          data: {
            subscribe: (fn: (value: Data) => void) => fn({
              yourData: 'yolo'
            })
          }
        }
      },
      FooterComponent
    ]
  }));

  it('should not log ngOnInit', inject([FooterComponent], (component: FooterComponent) => {
    spyOn(console, 'log');
    component.ngOnInit();
    expect(console.log).not.toHaveBeenCalled();
  }));

});

import { Component } from '@angular/core';
import { AuthService } from '../../../services/auth/auth.service';

@Component({
  selector: 'no-campaigns',
  templateUrl: './no-campaigns.component.html'
})
export class NoCampaignsComponent {
  constructor(private _authService: AuthService) {
  }

  ngOnInit() {
  }

  get loggedUser(): any {
    return this._authService.currentUserData;
  };
}

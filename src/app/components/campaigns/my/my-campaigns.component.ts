import { Component } from '@angular/core';
import { ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core/index';
import { CampaignService } from '../../../services/campaign/campaign.service';
import { Campaign } from '../../../models/campaign';

@Component({
  selector       : 'my-campaigns-page',
  // styleUrls      : ['./my-campaigns.css'],
  templateUrl    : './my-campaigns.template.html',
  providers      : [],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MyCampaignsPage {
  public cssStatuses: any         = {
    draft     : 'draft',
    pending   : 'pending',
    ready     : 'running',
    paused    : 'paused',
    low_credit: 'paused',
    completed : 'completed',
    archived  : 'gray'
  };
  public items: Campaign[];
         loadingFinished: boolean = false;

  constructor(private service: CampaignService,
              private cd: ChangeDetectorRef) {
  }

  getList(): void {
    let requestParams = {
      page: {
        number: 1,
        size  : 1000
      }
    };
    let subs          = this.service.getList(requestParams).subscribe(
      (items: any[]) => {
        this.items = items.sort((a, b) => b.startDate - a.startDate);
        this.cd.markForCheck();
        this.loadingFinished = true;
      },
      err => err,
      () => subs.unsubscribe()
    );
  }

  ngOnInit(): void {
    this.getList();
  }
}

import { Component, ViewChild } from '@angular/core';
import { ModalDirective } from 'ng2-bootstrap';
import { CampaignCall } from '../../../../models/campaign-call';

@Component({
  selector: 'modal-call-note',
  templateUrl: './call-note.modal.html'
})
export class ModalCallNoteComponent {
  @ViewChild('callNoteModal') modal: ModalDirective;

  call: CampaignCall;

  public show(call: CampaignCall): void {
    this.call = call;
    this.modal.show();
  }

  public hide(): void {
    this.modal.hide();
  }
}

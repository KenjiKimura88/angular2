import { Component, OnInit, OnDestroy } from '@angular/core';
import { Campaign, CampaignTypesByCategory } from '../../../models/campaign';
import { CampaignService } from '../../../services/campaign/campaign.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '../../../services/translate/translate.service';
import { Subscription } from 'rxjs';

@Component({
  selector   : 'campaign-create',
  templateUrl: './campaign-create.component.html'
})
export class CampaignCreateComponent implements OnInit, OnDestroy {
  campaign: Campaign;
  campaignForm: FormGroup;
  categories: any = CampaignTypesByCategory;
  subs: Subscription;

  constructor(private service: CampaignService,
              private formBuilder: FormBuilder,
              private router: Router,
              private translateService: TranslateService) {
  }

  ngOnInit(): void {
    this.campaignForm = this.formBuilder.group({
      name        : ['', Validators.required],
      kind        : ['', Validators.required],
      instructions: ['', [Validators.required, Validators.minLength(5)]]
    });

    // TODO, find the method to retrieve only ids
    let subs = this.service.getList().subscribe(
      items => {
        let campaignsCount = items.length + 1;
        this.campaignForm.patchValue({name: this.translateService.translate('My campaign') + ' ' + campaignsCount});
      },
      err => err, // TODO, add toastr
      () => subs.unsubscribe()
    );
  }

  createCampaign(): void {
    // TODO, find a solution to map the object
    let newCampaign = {
      name        : this.campaignForm.value.name,
      kind        : this.campaignForm.value.kind,
      instructions: this.campaignForm.value.instructions
    };

    this.subs = this.service.updateItem(newCampaign).subscribe(
      (item: Campaign) => {
        this.campaign = item;

        this.router.navigateByUrl('/campaigns/' + this.campaign.id + '/edit/express');
      },
      err => err, // TODO, add toastr
    );
  }

  scroll2(): void {
    let to = document.documentElement.scrollHeight - 60 - 55 - 200;
    window.scrollTo(0, to);
  }

  ngOnDestroy(): void {
    if (this.subs) {
      this.subs.unsubscribe();
    }
  }
}

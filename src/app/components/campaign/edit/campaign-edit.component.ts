import { Component, OnInit } from '@angular/core';
import { Campaign } from 'app/models/campaign';
import { Router, ActivatedRoute } from '@angular/router';
import { CampaignService } from '../../../services/campaign/campaign.service';
import { AuthService } from '../../../services/auth/auth.service';
import { CampaignPaymentService } from '../../../services/campaign/campaign.payment.service';
import { CampaignStatusService } from '../../../services/campaign/campaign-status.service';

@Component({
  selector   : 'campaign-edit',
  templateUrl: './campaign-edit.component.html'
})
export class CampaignEditComponent implements OnInit {
  campaign: Campaign;
  selectedCampaign: Campaign;
  public currentUserData: any = null;

  paid: boolean = true;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private service: CampaignService,
              private authService: AuthService,
              private campaignPaymentService: CampaignPaymentService,
              private campaignStatusService: CampaignStatusService) {
  }

  ngOnInit(): void {
    this.campaign = this.route.snapshot.data['campaign'];
    this.authService.waitForCurrentUserData().then((currentUserData) => {
      this.currentUserData = currentUserData;
      console.log(this.currentUserData);
    });
  }

  launchCampaign(): void {
    // we will redirect to payment page if the client is not paid yet
    let subs = this.service.updateItem(this.campaign).subscribe(
      (item: Campaign) => {
        // we will test here if the client paid already
        let data = {
          companyId : this.currentUserData.companyId
        };
        this.campaignPaymentService.getSubscription(data)
          .then((result) => {
            if (result.data) {
              this.selectedCampaign = this.campaign;
              let status: any = 'run';
              this.selectedCampaign.status = status;
              this.campaignStatusService.updateStatus(this.selectedCampaign)
                .then((res) => {
                  this.campaign.status = res.status;
                  localStorage.setItem('createdSubscription', 'true');
                  this.router.navigateByUrl('/campaigns/' + this.campaign.id);
                })
                .catch((error) => {
                  console.log('The status of Campaign was not updated');
                });
            } else {
              this.router.navigateByUrl('/campaigns/' + this.campaign.id + '/pay');
            }
          })
          .catch((error) => {});
      },
      err => err, // TODO, add toastr
      () => subs.unsubscribe()
    );
  }

  saveCampaign(): void {
    let subs = this.service.updateItem(this.campaign).subscribe(
      (item: Campaign) => this.router.navigateByUrl('/campaigns'),
      err => err, // TODO, add toastr
      () => subs.unsubscribe()
    );
  }
}

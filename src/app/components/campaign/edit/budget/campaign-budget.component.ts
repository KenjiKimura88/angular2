import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Logger } from 'angular2-logger/core';

import { TranslateService } from '../../../../services/translate/translate.service';
import { CampaignBudgetService } from '../../../../services/campaign/campaign.budget.service';

import { Campaign } from '../../../../models/campaign';
import { UtilsService } from '../../../../services/utils.service';

@Component({
  selector   : 'campaign-budget',
  templateUrl: './campaign-budget.component.html'
})
export class BudgetComponent implements OnInit {
  campaign: Campaign;
  acceptCommission: boolean;

  constructor(private route: ActivatedRoute,
              private toastr: ToastsManager,
              private router: Router,
              private translateService: TranslateService,
              private campaignBudgetService: CampaignBudgetService,
              private utilsService: UtilsService,
              private logger: Logger) {
  }

  ngOnInit(): void {
    this.campaign = this.route.snapshot.parent.parent.data['campaign'];

    if (!this.campaign.campaignBudget) {
      this.campaign.campaignBudget            = this.campaignBudgetService.getNewModel();
      this.campaign.campaignBudget.campaignId = this.campaign.id;
    }

    this.acceptCommission = !!this.campaign.campaignBudget.commission;
  }

  close(): void {
    let segmentedUrl = this.utilsService.getUrlByFirstSegments(this.router.url, 4);

    this.router.navigateByUrl(segmentedUrl);
  }

  update(): void {
    if (!this.acceptCommission) {
      this.campaign.campaignBudget.commission = 0;
    }

    let subs = this.campaignBudgetService.updateItem(this.campaign.campaignBudget).subscribe(
      (item: any) => {
        this.campaign.campaignBudget = item;
        this.toastr.success(this.translateService.translate('Campaign budget saved'));
      },
      err => this.logger.error('Error while saving campaign budget', err),
      () => subs.unsubscribe()
    );
  }

  save(): void {
    this.update();
    this.close();
  }
}

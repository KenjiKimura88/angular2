import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Campaign } from '../../../../models/campaign';
import { BudgetExpenseType } from '../../../../models/campaign-budget';

@Component({
  selector   : 'campaign-settings',
  templateUrl: './campaign-settings.component.html'
})
export class CampaignSettingsComponent implements OnInit {
  campaign: Campaign;
  budgetExpenseType: any = BudgetExpenseType;

  scriptsCompleted: boolean    = false;
  contactsCompleted: boolean   = false;
  campaignerCompleted: boolean = true;
  callersCompleted: boolean    = true;
  budgetCompleted: boolean     = false;
  timeCompleted: boolean       = true;
  resourcesCompleted: boolean  = true;

  // TODO, get this from a service or database
  campaignTips: Array<string> = [ // Referred to as 'help and tips array'
    'Tip 1',
    'Tip 2',
    'Tip 3',
    'Tip 4',
  ];

  constructor(private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.campaign          = this.route.snapshot.parent.parent.data['campaign'];
    console.log('settings campaign', this.campaign);
    this.scriptsCompleted  = !!this.campaign.numberOfQuestions;
    this.contactsCompleted = !!this.campaign.numberOfContacts;
    this.budgetCompleted   = !!this.campaign.campaignBudget;
  }
}

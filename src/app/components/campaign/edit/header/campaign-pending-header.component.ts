import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Campaign, CampaignTypesByCategory } from 'app/models/campaign';

@Component({
  selector: 'campaign-pending-header',
  templateUrl: './campaign-pending-header.component.html'
})
export class CampaignPendingHeaderComponent implements OnInit {
  @Input() campaign: Campaign;
  @Input() paid: boolean;

  @Output() launch: EventEmitter<any> = new EventEmitter();
  @Output() save: EventEmitter<any> = new EventEmitter();

  categoryTypes;
  scriptsCompleted: boolean = false;
  contactsCompleted: boolean = false;
  campaignerCompleted: boolean = true;
  launching: boolean = false;

  constructor() {
  }

  ngOnInit() {
    this.categoryTypes = this.extractCategoryTypes();

    this.scriptsCompleted = !!this.campaign.numberOfQuestions;
    this.contactsCompleted = !!this.campaign.numberOfContacts;
    // this.campaignerCompleted = !!this.campaign.callerId;
  }

  launchCampaign() {
    this.launching = true;
    this.launch.emit(true);
  }

  saveCampaign() {
    this.save.emit(true);
  }

  extractCategoryTypes() {
    let sameTypes = {};

    CampaignTypesByCategory.map(
      category => {
        for (let type of Object.keys(category.types)) {
          if (type === <string>this.campaign.kind) {
            sameTypes = category.types;

            break;
          }
        }
      }
    );

    return sameTypes;
  }
}

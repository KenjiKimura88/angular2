import { Component, OnInit, Input } from '@angular/core';
import { Campaign } from 'app/models/campaign';
import { CampaignStatusService } from '../../../../services/campaign/campaign-status.service';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Logger } from 'angular2-logger/core';

@Component({
  selector: 'campaign-not-pending-header',
  templateUrl: './campaign-not-pending-header.component.html'
})
export class CampaignNotPendingHeaderComponent implements OnInit {
  @Input() campaign: Campaign;
  selectedCampaign: Campaign;

  constructor(private toastr: ToastsManager,
              private logger: Logger,
              private campaignStatusService: CampaignStatusService) {
  }

  ngOnInit(): void {
  }

  onExportChange(exportValue: string): void {
    if (exportValue === 'CSV') {
      window.open(`https://www.upcall.com/en/campaigns/${this.campaign.id}/calls.csv`);
    }
  }

  onStatusChange(selectedStatus: any): void {
    this.selectedCampaign = this.campaign;
    this.selectedCampaign.status = selectedStatus;

    this.campaignStatusService.updateStatus(this.selectedCampaign)
      .then((result) => {
          this.toastr.success('The status of Campaign was updated successfully.');
          this.campaign.status = result.status;
        })
        .catch((error) => {
          this.toastr.info('The status of Campaign was not updated unfortunately.');
          this.logger.error(error);
        });
  }
}

import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { CampaignCallersService } from '../../../../services/campaign/campaign-callers.service';
import { CampaignCaller, CampaignCallerStatuses } from '../../../../models/campaign-caller';
import { Campaign } from '../../../../models/campaign';
import { CampaignSettings } from '../../../../models/campaign-settings';
import { Logger } from 'angular2-logger/core';
import { CampaignSettingsService } from '../../../../services/campaign/campaign.settings.service';
import { TranslateService } from '../../../../services/translate/translate.service';
import { ModalRejectCallerComponent } from '../../modals/caller-reject/caller-reject.modal';

import * as _ from 'lodash';

@Component({
  selector   : 'campaign-callers',
  templateUrl: './campaign-callers.component.html'
})
export class CampaignCallersComponent implements OnInit {
  selectedCaller: CampaignCaller;
  callers: CampaignCaller[];
  campaign: Campaign;
  campaignSettings: CampaignSettings;

  currentPage: number  = 1;
  itemsPerPage: number = 10;
  nextExists: boolean  = false;

  currentStatus: string = 'All contacts';
  statuses: Object      = CampaignCallerStatuses;
  toBeApproved: Object  = {
    approve: 'Approve',
    reject : 'Reject'
  };

  sorts: any = {
    name        : {
      label   : 'Name',
      selected: false,
      dir     : 'asc',
      param   : 'caller.user.first_name'
    },
    rating      : {
      label   : 'Rating',
      selected: false,
      dir     : 'asc',
      param   : 'rating.rate'
    },
    totalCalls  : {
      label   : 'Tries',
      selected: false,
      dir     : 'asc',
      param   : 'total_calls'
    },
    talkingCalls: {
      label   : 'Calls answered',
      selected: false,
      dir     : 'asc',
      param   : 'talking_calls'
    },
    successCalls: {
      label   : 'Script completed',
      selected: false,
      dir     : 'asc',
      param   : 'success_calls'
    }
  };

  @ViewChild(ModalRejectCallerComponent) rejectCallerModal: ModalRejectCallerComponent;

  constructor(private campaignCallersService: CampaignCallersService,
              private route: ActivatedRoute,
              private toastr: ToastsManager,
              private logger: Logger,
              private campaignSettingsService: CampaignSettingsService,
              private translateService: TranslateService) {
  }

  ngOnInit(): void {
    this.campaign                 = this.route.snapshot.parent.parent.data['campaign'];
    this.campaign.campaignSetting = this.campaign.campaignSetting || this.campaignSettingsService.getNewModel();

    this.getList();
    this.updateMainList();
  }

  getList(): void {
    let subs = this.campaignCallersService.getCampaignCallers({id: this.campaign.id}).subscribe(
      (items: any[]) => this.callers = items,
      err => this.logger.error('Error while fetching callers list', err),
      () => subs.unsubscribe()
    );
  }

  updateMainList(): void {
    let params = {
      id  : this.campaign.id,
      page: {number: this.currentPage, size: this.itemsPerPage}
    };
    let sort   = Object.keys(this.sorts)
      .filter(k => this.sorts[k].selected)
      .map(k => this.sorts[k].dir === 'asc' ? this.sorts[k].param : '-' + this.sorts[k].param)
      .join(',');

    if (sort) {
      params['sort'] = sort;
    }

    let subs = this.campaignCallersService.getCampaignCallers(params).subscribe(
      (items: any[]) => this.callers = items,
      err => this.logger.error('Error while fetching filtered callers list', err),
      () => subs.unsubscribe()
    );
  }

  selectCaller(caller: CampaignCaller): void {
    this.selectedCaller = caller;
  }

  closeUserInfo(): void {
    this.selectedCaller = undefined;
  }

  changePage(navData: any): void {
    this.currentPage  = navData.page;
    this.itemsPerPage = navData.itemsPerPage || this.itemsPerPage;
    this.updateMainList();
  }

  sortBy(sortObj: any): void {
    if (sortObj.selected) {
      // sort asc
      // sort desc
      // remove sorting
      if (sortObj.dir === 'asc') {
        sortObj.dir = 'desc';
      } else {
        sortObj.dir      = 'asc';
        sortObj.selected = false;
      }
    } else {
      sortObj.selected = true;
    }
    this.updateMainList();
  }

  statusesKeys(): Array<string> {
    return Object.keys(this.statuses);
  }

  onDeleteCaller(caller: CampaignCaller): void {
    let idxCaller = _.findIndex(this.callers, {'id': caller.id});
    this.callers.splice(idxCaller, 1);
    this.selectCaller(this.callers[idxCaller]);
  }

  updateCampaignSettings(value: string): void {
    if (!this.campaign.campaignSetting.campaignId) {
      this.campaign.campaignSetting.campaignId = +this.campaign.id;
    }

    this.campaign.campaignSetting.approvedCallers = (value === '1') ? true : false;

    let subs = this.campaignSettingsService.updateItem(this.campaign.campaignSetting).subscribe(
      (item: any) => this.toastr.success(this.translateService.translate('Campaign settings saved')),
      err => err,
      () => subs.unsubscribe()
    );
  }

  callerApproval(status: string, caller: CampaignCaller): void {
    if (status === 'reject') {
      this.rejectCallerModal.show();
    } else {
      this.updateCaller(status, caller);
    }
  }

  updateCaller(status: string, caller: CampaignCaller, removalReason: string = null, removalOtherReason: string = null): void {
    caller.status = status;
    caller.removalReason = removalReason;
    caller.removalOtherReason = removalOtherReason;

    let translatedStatus = this.translateService.translate(status + 'ed');
    if (status === 'remove') {
      translatedStatus = 'removed';
      caller.status = 'reject';
    }

    let subs = this.campaignCallersService.updateStatus(caller, removalReason, removalOtherReason).subscribe(
      (item: any) => {
        this.selectedCaller = (this.selectedCaller && item.status !== 'rejected') ? item : undefined;
        this.updateMainList();
        this.toastr.success(
          this.translateService.translate('Campaign caller was successfully %s', {params: [translatedStatus]})
        );
      },
      err => err,
      () => subs.unsubscribe()
    );
  }
}

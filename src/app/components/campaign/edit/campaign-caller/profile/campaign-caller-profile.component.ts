import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { ToastsManager } from 'ng2-toastr';
import { CampaignCaller } from '../../../../../models/campaign-caller';
import { CampaignCallersService } from '../../../../../services/campaign/campaign-callers.service';
import { CallersService } from '../../../../../services/callers.service';
import { AuthService } from '../../../../../services/auth/auth.service';
import { ModalRejectCallerComponent } from '../../../modals/caller-reject/caller-reject.modal';
import { TranslateService } from '../../../../../services/translate/translate.service';
import { Rating } from '../../../../../models/rating';

import * as moment from 'moment-timezone';
import * as Languages from 'language-list';
import { Router } from '@angular/router';
import { UtilsService } from '../../../../../services/utils.service';

@Component({
  selector   : 'campaign-caller-profile',
  templateUrl: './campaign-caller-profile.component.html'
})
export class CampaignCallerProfileComponent implements OnInit {
  averageRating: number;
  localTime: string;
  ratings: Array<Rating>;
  languages: any = Languages();
  industryShowLimit: number = 10;
  hasIndustryShowLimit: boolean = true;

  @Input() campaignCaller: CampaignCaller;
  @ViewChild(ModalRejectCallerComponent) rejectCallerModal: ModalRejectCallerComponent;

  constructor(private router: Router,
              private campaignCallersService: CampaignCallersService,
              private callersService: CallersService,
              private utilsService: UtilsService,
              private _authService: AuthService,
              private toastr: ToastsManager,
              private translateService: TranslateService) {
  }

  ngOnInit(): void {
    this.getCallerRatings(this.campaignCaller.id);
    this.localTime = this.getLocalTime();
  }

  get chanelId(): any {
    if (this._authService.currentUserData && this.campaignCaller.user) {
      return `${this._authService.currentUserData.id},${this.campaignCaller.user.id}`;
    } else {
      return '';
    }
  }

// loggedUser?.id + ',' + campaignCaller.user?.id

  get loggedUser(): any {
    return this._authService.currentUserData;
  };

  goBack(): void {
    let segmentedUrl = this.utilsService.getUrlByFirstSegments(this.router.url, 4);

    this.router.navigateByUrl(segmentedUrl);
  }

  rejectCaller(removalReason: string, removalOtherReason: string = null, action: string = 'reject'): void {
    this.campaignCaller.status = 'reject';
    this.campaignCaller.removalReason = removalReason;
    this.campaignCaller.removalOtherReason = removalOtherReason;

    let subs = this.campaignCallersService.updateStatus(this.campaignCaller, removalReason, removalOtherReason).subscribe(
      (item: any) => {
        this.toastr.success(
          this.translateService.translate('Campaign caller was successfully removed')
        );
        this.goBack();
      },
      err => err,
      () => subs.unsubscribe()
    );
  }

  // TODO update after API implementation
  getCallerRatings($callerId: string): void {
    let subs = this.callersService.getItem($callerId).subscribe(
      (item: any) => {
        this.ratings       = item.ratings;
        this.averageRating = item.getAverageRating();
      },
      err => err,
      () => subs.unsubscribe()
    );
  }

  getLocalTime(): string {
    let timezone = '';

    if (this.campaignCaller.caller) {
      timezone = moment().tz(this.campaignCaller.caller.timezone).format('h:mm a');
    }

    return timezone;
  }

  onClickShowMoreOrLessIndustries(): void {
    this.hasIndustryShowLimit = !this.hasIndustryShowLimit;
  }
}

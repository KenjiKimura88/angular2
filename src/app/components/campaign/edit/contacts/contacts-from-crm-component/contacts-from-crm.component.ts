import { Component, OnInit } from '@angular/core';
import { CampaignSettingsService } from '../../../../../services/campaign/campaign.settings.service';
import { Campaign } from '../../../../../models/campaign';
import { ActivatedRoute, Router } from '@angular/router';
import { CampaignService } from '../../../../../services/campaign/campaign.service';
import { TranslateService } from '../../../../../services/translate/translate.service';
import { Logger } from 'angular2-logger/core';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { UtilsService } from '../../../../../services/utils.service';

@Component({
  selector   : 'contacts-from-crm',
  templateUrl: './contacts-from-crm.component.html'
})
export class ContactsFromCrmComponent implements OnInit {
  campaign: Campaign;

  importLink: string;

  formValues: any = {
    scrubForDoNotCall : false,
    campaignAlwaysOpen: false
  };

  crmSources: any = [
    {
      name : 'Asana',
      image: 'asana.png'
    },
    {
      name : 'AutoPilot',
      image: 'autopilot.png'
    },
    {
      name : 'Base',
      image: 'base.png'
    },
    {
      name : 'Close.io',
      image: 'closeio.png'
    },
    {
      name : 'Dropbox',
      image: 'dropbox.png'
    },
    {
      name : 'Eventbrite',
      image: 'eventbrite.png'
    },
    {
      name : 'Evernote',
      image: 'evernote.png'
    },
    {
      name : 'Excel',
      image: 'excel.png'
    },
    {
      name : 'FreshDesk',
      image: 'freshdesk.png'
    },
    {
      name : 'Google Drive',
      image: 'googledrive.png'
    },
    {
      name : 'Google Sheets',
      image: 'googlesheets.png'
    },
    {
      name : 'Highrise',
      image: 'highrise.png'
    },
    {
      name : 'Hubspot',
      image: 'hubspot.png'
    },
    {
      name : 'Intercom',
      image: 'intercom.png'
    },
    {
      name : 'MailChimp',
      image: 'mailchimp.png'
    },
    {
      name : 'Marketo',
      image: 'marketo.png'
    },
    {
      name : 'MySQL',
      image: 'mysql.png'
    },
    {
      name : 'Office 365',
      image: 'office365.png'
    },
    {
      name : 'Pardot',
      image: 'pardot.png'
    },
    {
      name : 'Pipedrive',
      image: 'pipedrive.png'
    },
    {
      name : 'Salesforce',
      image: 'salesforce.png'
    },
    {
      name : 'Sendgrid',
      image: 'sendgrid.png'
    },
    {
      name : 'Shopify',
      image: 'shopify.png'
    },
    {
      name : 'Slack',
      image: 'slack.png'
    },
    {
      name : 'SugarCRM',
      image: 'sugarcrm.png'
    },
    {
      name : 'Surveymonkey',
      image: 'surveymonkey.png'
    },
    {
      name : 'Typeform',
      image: 'typeform.png'
    },
    {
      name : 'Wufoo',
      image: 'wufoo.png'
    },
    {
      name : 'Zendensk',
      image: 'zendensk.png'
    },
    {
      name : 'Zoho',
      image: 'zoho.png'
    },
  ];

  constructor(private route: ActivatedRoute,
              private campaignSettingsService: CampaignSettingsService,
              private campaignService: CampaignService,
              private utilsService: UtilsService,
              private toastr: ToastsManager,
              private translateService: TranslateService,
              private logger: Logger,
              private router: Router) {
  }

  ngOnInit(): void {
    this.campaign                      = this.route.snapshot.parent.parent.parent.data['campaign'];
    this.campaign.campaignSetting      = this.campaign.campaignSetting || this.campaignSettingsService.getNewModel();
    this.formValues.scrubForDoNotCall  = this.campaign.campaignSetting.scrub;
    this.formValues.campaignAlwaysOpen = this.campaign.alwaysOpen;

    this.buildImportLink();
  }

  saveDataForCampaignForContactsFromCrm(): void {
    this.campaign.alwaysOpen = this.formValues.campaignAlwaysOpen;
    this.saveCampaign(this.campaign);

    this.updateCampaignSettings(
      {
        scrubForDoNotCall: this.formValues.scrubForDoNotCall
      }
    )
      .then(() => {
        let urlTree    = this.router.parseUrl(this.router.url);
        let location   = (urlTree.root.children as any).primary.segments[3];

        this.router.navigateByUrl(`/campaigns/${this.campaign.id}/edit/${location}`);
      });
  }

  updateCampaignSettings(settingsToUpdate: any): Promise<any> {
    this.campaign.campaignSetting.scrub = settingsToUpdate['scrubForDoNotCall'] ? settingsToUpdate['scrubForDoNotCall'] : false;

    if (!this.campaign.campaignSetting.campaignId) {
      this.campaign.campaignSetting.campaignId = +this.campaign.id;
    }

    return this.campaignSettingsService.updateItem(this.campaign.campaignSetting).toPromise();
  }

  setCampaignAlwaysOpen(flag: boolean): void {
    this.formValues.campaignAlwaysOpen = flag;
  }

  saveCampaign(campaign: Campaign): void {
    this.campaignService.updateItem(campaign).toPromise()
      .then((result: Campaign) => {
        this.toastr.info(this.translateService.translate('Campaign was saved'));
      })
      .catch((result) => {
        this.toastr.error(this.translateService.translate('Error saving campaign'));
        this.logger.error('Error saving campaign', result);
      });
  }

  close(): void {
    let segmentedUrl = this.utilsService.getUrlByFirstSegments(this.router.url, 4);

    this.router.navigateByUrl(segmentedUrl);
  }

  buildImportLink(): void {
    let segmentedUrl = this.utilsService.getUrlByFirstSegments(this.router.url, 5);

    this.importLink = `${segmentedUrl}/import`;
  }
}

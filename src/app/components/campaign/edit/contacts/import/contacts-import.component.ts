import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

import { CampaignService } from '../../../../../services/campaign/campaign.service';
import { CampaignSettingsService } from '../../../../../services/campaign/campaign.settings.service';
import { CampaignContactService } from '../../../../../services/campaign/campaign.contact.service';
import { TranslateService } from '../../../../../services/translate/translate.service';

import { ModalContactImportComponent } from '../../../modals/contact-import/contact-import.modal';
import { ModalErrorsComponent } from '../../../modals/errors/errors.modal';

import { Campaign } from '../../../../../models/campaign';

import * as _ from 'lodash';
import * as XLSX from 'xlsx';
import * as CSV from 'papaparse';
import { UtilsService } from '../../../../../services/utils.service';

@Component({
  selector   : 'contacts-import',
  templateUrl: './contacts-import.component.html'
})
export class ContactsImportComponent implements OnInit {
  campaign: Campaign;
  campaigns: Campaign[];

  fileName: string;
  validFile: boolean            = false;
  validMimeTypes: Array<string> = [
    'application/excel',
    'application/vnd.ms-excel',
    'application/x-excel',
    'application/x-msexcel',
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    'text/csv'
  ];

  databaseColumns: Object = {
    number     : 'Phone number',
    extension  : 'Phone extention',
    first_name : 'First name',
    last_name  : 'Last name',
    title      : 'Title',
    email      : 'Email',
    company    : 'Company name',
    line1      : 'Address',
    city       : 'City',
    postal_code: 'Postal code',
    region     : 'Region',
    country    : 'Country',
    info       : 'Contact info',
    website    : 'Website',
    key        : 'Other'
  };

  jsonDataStructure: Object = {
    contact      : {
      'first_name': '',
      'last_name' : '',
      'info'      : '',
      'title'     : '',
      'company'   : '',
      'email'     : ''
    },
    phone        : {
      'number'   : '',
      'extension': ''
    },
    address      : {
      'line1'      : '',
      'city'       : '',
      'region'     : '',
      'postal_code': '',
      'country'    : ''
    },
    social_medium: {
      'website': ''
    },
    keyvalues    : []
  };

  jsonStructure: Object = {
    campaign_id   : '',
    operation_type: '',
    merge_type    : '',
    data          : []
  };

  parsedFileData: Array<Object>;
  fileColumnNames: Array<string>         = [];
  extractedDataAfterMatch: Array<Object> = [];
  /** I use this var to create a stack of all available obtions for each column match select */
  availableColumnsStack: Object          = {};

  parsing: boolean               = false;
  canMatch: boolean              = false;
  dataPreparedForImport: boolean = false;
  importDone: boolean            = false;
  importErrors: any;

  nrContactsImported: number = 0;

  crmLink: string;

  /** ModalContactImportComponent is strong dependent on ContactsImportComponent */
  @ViewChild(ModalContactImportComponent) private contactImportModal: ModalContactImportComponent;
  @ViewChild(ModalErrorsComponent) private errorsModal: ModalErrorsComponent;

  constructor(private campaignService: CampaignService,
              private campaignSettingsService: CampaignSettingsService,
              private route: ActivatedRoute,
              private router: Router,
              private toastr: ToastsManager,
              private translateService: TranslateService,
              private campaignContactService: CampaignContactService,
              private utilsService: UtilsService) {
  }

  ngOnInit(): void {
    this.campaign                 = this.route.snapshot.parent.parent.parent.data['campaign'];
    this.campaign.campaignSetting = this.campaign.campaignSetting || this.campaignSettingsService.getNewModel();
    this.getCampaignList();
    this.buildCrmLink();
  }

  getCampaignList(): void {
    let subs = this.campaignService.getList().subscribe(
      (items: any[]) => this.campaigns = items,
      err => err,
      () => subs.unsubscribe()
    );
  }

  updateCampaignSettings(values: Array < string >): void {
    this.campaign.campaignSetting.scrub = values['scrub'] ? values['scrub'] : false;

    if (!this.campaign.campaignSetting.campaignId) {
      this.campaign.campaignSetting.campaignId = +this.campaign.id;
    }

    this.campaignSettingsService.updateItem(this.campaign.campaignSetting);

    delete values['scrub'];
  }

  close(): void {
    let segmentedUrl = this.utilsService.getUrlByFirstSegments(this.router.url, 4);

    this.router.navigateByUrl(segmentedUrl);
  }

  defaultPreventer(event: any): void {
    event.stopPropagation();
    event.preventDefault();
  }

  onDrop(event: any): void {
    this.defaultPreventer(event);
    this.parseFile(event.dataTransfer.files);
  }

  handleFile(event: any): void {
    this.defaultPreventer(event);
    this.parseFile(event.target.files);
  }

  parseFile(files: any): void {
    this.parsing = true;

    if (files.length !== 1) {
      this.toastr.error(this.translateService.translate('Please add one file to parse'));
      return;
    }

    let file      = files[0]; // we accept only one file
    this.fileName = file.name;

    if (!this.isValidMimeType(file.type)) {
      this.toastr.error(this.translateService.translate('Please upload only xls, xlsx or csv files'));
      return;
    }

    this.loadFileData(file);
  }

  loadFileData(file: any): void {
    let reader = new FileReader();

    reader.onload = (event) => {
      let data = (<any>event.target).result;

      this.parsedFileData = (file.type === 'text/csv') ? this.parseCsv(data) : this.parseXls(data);

      if (!(this.parsedFileData.length > 0)) {
        this.toastr.error(this.translateService.translate('There is no valid data in %s', this.fileName));
        this.reset();

        return;
      }

      this.fillAvailableColumnsStack();

      this.validFile = true;

      this.parsing = false;
    };

    reader.readAsBinaryString(file);
  }

  parseXls(data: any): Array < Object > {
    let parsedData    = [];
    let workbook      = XLSX.read(data, {type: 'binary'});
    let worksheetName = workbook.SheetNames[0]; // take only first worksheet

    if (workbook.Sheets.hasOwnProperty(worksheetName)) {
      let worksheet = workbook.Sheets[worksheetName];
      parsedData    = (XLSX as any).utils.sheet_to_row_object_array(worksheet); // get each row as array
    }

    return parsedData;
  }

  parseCsv(data: any): Array < Object > {
    let parsedData = CSV.parse(data, {header: true});

// remove rows from parsedData if parsing errors
    if (parsedData.errors && parsedData.errors.length) {
      for (let error of parsedData.errors) {
        parsedData.data.splice(error.row, 1);
      }
    }

    return parsedData.data;
  }

  isValidMimeType(type: string): Boolean {
    return (this.validMimeTypes.indexOf(type) !== -1);
  }

  fillAvailableColumnsStack(): void {
    this.fileColumnNames = Object.keys(this.parsedFileData[0]); // take column names

    for (let value of this.fileColumnNames) {
      if (value) { // done this because there are columns without names and values
        this.availableColumnsStack[value] = _.cloneDeep(this.databaseColumns);
      } else {
        this.fileColumnNames.splice(this.fileColumnNames.indexOf(value), 1);
      }
    }
  }

  getMatchingColumns(values: Array < string >): Array < string > {
    // cleanup empty values
    for (let key in values) {
      if (values.hasOwnProperty(key) && values[key] === '') {
        delete values[key];
      }
    }

    if (!Object.keys(values).length) {
      this.toastr.error(this.translateService.translate('There are no columns to be matched'));
      return;
    }

    return values;
  }

  extractDataAfterMatch(fileData: Array < Object >, matchingColumns: Object): Array < Object > {
    let extractedData = [];

    fileData.map(
      row => {
        let data = {
          keyvalues: []
        };

        _.each(matchingColumns, (dbCol, fileCol) => {
          let keyvalues = {
            'key'  : '',
            'value': ''
          };

          if (dbCol === 'key') {
            keyvalues.key   = fileCol;
            keyvalues.value = row[fileCol];

            data['keyvalues'].push(keyvalues);
          } else if (row.hasOwnProperty(fileCol)) {
            data[dbCol] = row[fileCol];
          }
        });

        extractedData.push(data);
      }
    );

    return extractedData;
  }

  prepareDataForImport(values: Array < string >): void {
    this.updateCampaignSettings(values);

    let matchingColumns          = this.getMatchingColumns(values);
    this.extractedDataAfterMatch = this.extractDataAfterMatch(this.parsedFileData, matchingColumns);

    if (this.campaign.numberOfContacts > 0) {
      this.contactImportModal.show(); // this will trigger the import with replace/merge option
    } else {
      this.postImport({});
    }

    this.dataPreparedForImport = true;
  }

  /** send POST Request with bulk JSON data structure */
  postImport(options: Object = {}): void {
    this.prepareJsonStructureForImport();

    if (options.hasOwnProperty('contactAction')) {
      this.jsonStructure['operation_type'] = options['contactAction'];
    } else {
      this.jsonStructure['operation_type'] = 'replace'; // set operation_type to comply with API JSON structure
    }

    if (options.hasOwnProperty('mergeType')) {
      this.jsonStructure['merge_type'] = options['mergeType'];
    }

    /** send Request */
    this.campaignContactService.bulkImport(this.jsonStructure)
      .then(response => {
          this.nrContactsImported = response.data.success;

          if (response.errors) {
            this.importErrors = response.errors;
            this.errorsModal.show();
          }
        }
      );
  }

  prepareJsonStructureForImport(): void {
    _.each(this.extractedDataAfterMatch, row => {
      let contactData = _.cloneDeep(this.jsonDataStructure);

      this.jsonStructure['campaign_id'] = this.campaign.id;

      _.each(this.jsonDataStructure, (dbKeys, structureKey) => {
        // populate keyvalues for each row
        if (row.hasOwnProperty('keyvalues')) {
          contactData['keyvalues'] = row['keyvalues'];
        }

        _.each(Object.keys(dbKeys), key => {
          if (row.hasOwnProperty(key)) {
            contactData[structureKey][key] = row[key];
          }
        });
      });

      this.jsonStructure['data'].push(contactData);
    });
  }

  reset(): void {
    this.dataPreparedForImport           = false;
    this.validFile                       = false;
    this.nrContactsImported              = 0;
    this.extractedDataAfterMatch         = [];
    this.availableColumnsStack           = {};
    this.canMatch                        = false;
    this.importDone                      = false;
    this.jsonStructure['campaign_id']    = '';
    this.jsonStructure['operation_type'] = '';
    this.jsonStructure['merge_type']     = '';
    this.jsonStructure['data']           = [];
    this.importErrors                    = [];
  }

  updateAvailableColumns(colKey: string, value: string, formValues: Object): void {
    /**
     * current selected option = value and is not shown in formValues list
     * set current selected option manualy
     * keep only selected options
     */
    let allSelectedValues = {};
    _.each(formValues, (formValue, key) => {
      // set selected option manually
      if (key === colKey && formValues.hasOwnProperty(key) && (formValue === '' || formValue !== value)) {
        allSelectedValues[key] = value;
        return;
      }

      // keep only selected options
      if (formValues.hasOwnProperty(key) && formValue !== '') {
        allSelectedValues[key] = formValue;
        return;
      }
    });

    let selectedDbColumns = _.values(allSelectedValues);
    this.canMatch         = (selectedDbColumns.indexOf('number') !== -1) ? true : false;

    _.each(this.availableColumnsStack,
      (dbColumns, xlsCol) => {
        let valuesToBeRemoved              = _.cloneDeep(allSelectedValues);
        this.availableColumnsStack[xlsCol] = _.cloneDeep(this.databaseColumns); // initial set of columns

        if (valuesToBeRemoved.hasOwnProperty(xlsCol)) { // skip if selected option exists for this column
          delete valuesToBeRemoved[xlsCol];
        }

        _.each(valuesToBeRemoved, (val: string) => {
          if (this.availableColumnsStack[xlsCol].hasOwnProperty(val)) {
            delete this.availableColumnsStack[xlsCol][val];
          }
        });
      }
    );
  }

  buildCrmLink(): void {
    let segmentedUrl = this.utilsService.getUrlByFirstSegments(this.router.url, 5);

    this.crmLink = `${segmentedUrl}/from-crm`;
  }
}

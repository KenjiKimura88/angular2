import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/operator/mergeMap';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { CampaignService } from '../../../../services/campaign/campaign.service';
import { CampaignSettingsService } from '../../../../services/campaign/campaign.settings.service';
import { FollowUpActionsService } from '../../../../services/campaign/follow-up-actions.service';
import { QuestionService } from '../../../../services/question/question.service';
import { QuestionFieldsService } from '../../../../services/question/question.fields.service';
import { Campaign } from '../../../../models/campaign';
import { CampaignSettings } from '../../../../models/campaign-settings';
import { FollowUpActions } from '../../../../models/follow-up-actions';
import { Question } from '../../../../models/question';
import { UtilsService } from '../../../../services/utils.service';

@Component({
  selector   : 'campaign-edit',
  templateUrl: './scripts-edit.component.html'
})
export class ScriptsEditComponent implements OnInit {
  public questions: any[];
  public originalQuestions: any[];
  public settings: CampaignSettings;
  public followUpActions: FollowUpActions;

  public responseTypes: any = [
    {
      type : 'multiple_checkbox',
      label: 'Multi',
      class: 'streamline-icon-check-box-2'
    },
    {
      type : 'multiple_radio',
      label: 'Unique',
      class: 'streamline-icon-unchecked-circle'
    },
    {
      type : 'stars',
      label: 'Rate',
      class: 'streamline-icon-star-full'
    },
    {
      type : 'freeform',
      label: 'Free text',
      class: 'streamline-icon-free-text'
    },
    {
      type : 'nps',
      label: 'NPS',
      class: 'streamline-icon-thumbs-up-1'
    }
  ];
  private campaign: Campaign;

  constructor(private service: QuestionService,
              private campaignService: CampaignService,
              private questionFieldsService: QuestionFieldsService,
              private campaignSettingsService: CampaignSettingsService,
              private followUpActionsService: FollowUpActionsService,
              private utilsService: UtilsService,
              private route: ActivatedRoute,
              private toastr: ToastsManager,
              private router: Router) {
  }

  ngOnInit(): void {
    this.campaign        = this.route.snapshot.parent.parent.data['campaign'];
    // this.campaign.extra_instructions = this.campaign.extraInstructions; // fix for a bug in the API
    this.followUpActions = this.campaign.getFollowUpAction();
    this.settings        = this.campaign.getSettings();

    if (this.settings && this.settings.id) {
      this.getSettings(this.settings.id);
    }

    if (this.followUpActions && this.followUpActions.id) {
      this.getFollowUpActions(this.followUpActions.id);
    }
    this.getList(this.campaign.id);
  }

  getList(id: string): void {
    let subs = this.service.getList({id}).subscribe(
      (questions: any[]) => {
        this.questions         = questions.sort((a, b) => a.position - b.position);
        this.originalQuestions = this.questions.slice();
      },
      err => err,
      () => subs.unsubscribe()
    );
  }

  getSettings(id: string): void {
    this.campaignSettingsService.getItem(id).subscribe(settings => {
      this.settings                = settings;
      this.settings.leaveVoicemail = this.settings.leaveVoicemail ? 1 : 0;
    });
  }

  getFollowUpActions(id: string): void {
    this.followUpActionsService.getItem(id).subscribe(actions => this.followUpActions = actions);
  }

  saveCampaign(): void {
    // this.toastr.info('Just some information for you.');
    this.campaignService.updateItem(this.campaign).toPromise()
      .then((item: Campaign) => {
        this.campaign = item;
        return Observable.forkJoin(
          this.saveSettings(),
          this.saveFollowUpActions()
        ).toPromise();
      }).then(() => this.saveQuestions())
      .then(() => {
        this.toastr.success('Campaign saved!');
        let segmentedUrl = this.utilsService.getUrlByFirstSegments(this.router.url, 4);
        this.router.navigateByUrl(segmentedUrl);
      }).catch(err => {
        this.toastr.error(err);
      });
  }

  saveSettings(): Observable<any> {
    if (this.settings && this.settings.id) {
      return this.campaignSettingsService.updateItem(this.settings);
    } else {
      return Observable.of(false);
    }
  }

  saveFollowUpActions(): Observable<any> {
    if (this.followUpActions && this.followUpActions.id) {
      return this.followUpActionsService.updateChildItem(this.followUpActions, this.campaign.id);
    } else {
      return Observable.of(false);
    }
  }

  deleteQuestions(): Promise<any> {
    // remove deleted questions fron this.originalQuestions
    // retrieve all questions from this.origQ that don't exist anymore in this.questions
    let toDelete          = this.originalQuestions.filter(q => {
      return !this.questions.some(q2 => q2.id === q.id);
    });
    let deleteObservables = toDelete.map(q => {
      // remove question
      return this.service.removeChildItem(q, this.campaign.id);
    });
    return Observable.forkJoin(...deleteObservables).toPromise();
  }

  saveQuestions(): Promise<any> {
    return this.deleteQuestions()
      .then(() => {
        // add all questions from this.questions
        let promises: any = this.questions.map(question => {
          question.response_type = question.responseType;
          return this.saveQuestionAndFields(question);
        });
        return Observable.forkJoin(promises).toPromise();
      });
  }

  saveQuestionAndFields(question: Question): Promise<any> {
    return this.service.updateChildItem(question, this.campaign.id).toPromise()
      .then(res => {
        question.id = res.id;
        return this.saveQuestionFields(question.questionFields, question);
      });

  }

  saveQuestionFields(fields: any, question: any): Promise<any> {
    let originalQuestion = this.originalQuestions.filter(q => {
        return q.id === question.id;
      })[0] || this.questionFieldsService.getNewModel();

    originalQuestion.questionFields = originalQuestion.questionFields || [];
    let toDelete                    = originalQuestion.questionFields.filter(f => {
        return !fields.some(f2 => f2.id === f.id);
      }) || [];
    let toDeleteObservables         = toDelete.map(f => {
      // remove field
      return this.questionFieldsService.removeChildItem(f, question.id).toPromise();
    });

    return Observable.forkJoin(...toDeleteObservables).toPromise()
      .then(() => {
        // add all fields
        fields               = fields || [];
        let fieldObservables = fields.map(field => {
          return this.questionFieldsService.updateChildItem(field, question.id).toPromise()
            .then(fieldResponse => field.id = fieldResponse.id);
        });
        return Observable.forkJoin(fieldObservables).toPromise();
      });
  }

  addOption(question: any): void {
    question.questionFields = question.questionFields || [];
    question.questionFields.push({
      question_id: this.campaign.id,
      field      : '',
      position   : question.questionFields.length,
    });
  }

  removeOption(option: any, question: any): void {
    question.questionFields = question.questionFields.filter(field => {
      return !(field === option);
    });
  }

  addQuestion(): void {
    this.questions.push({
      question: '',
      responseType: 'multiple_checkbox',
      explanations: '',
      comment: '',
      campaignId: this.campaign.id,
      position: this.questions.length + 1,
      questionFields: [],
    });
  }

  removeQuestion(question: any): void {
    this.questions = this.questions.filter(q => {
      return !(q.position === question.position);
    });
    // recompute positions
    this.questions = this.questions.map(q => {
      if (q.position > question.position) {
        q.position--;
      }
      return q;
    });
  }

  // insert the duplicated question immediately after the original one
  duplicateQuestion(question: any): void {
    let duplicate = {
      question: question.question,
      responseType: question.responseType,
      explanations: question.explanations,
      comment: question.comment,
      campaignId: question.campaignId,
      position: question.position,
      questionFields: [],
    };

    for (let qf of question.questionFields) {
      duplicate.questionFields.push({
        question_id: qf.campaignId,
        field: qf.field,
        position: qf.position,
      });
    }

    this.questions = this.questions.map(q => {
      if (q.position > duplicate.position) {
        q.position++;
      }
      return q;
    });
    duplicate.position++;

    this.questions.push(duplicate);
    this.questions = this.questions.sort((a, b) => a.position - b.position);
  }

  moveUp(question: any): void {
    this.questions = this.questions.map(q => {
      if (q.position === question.position - 1) {
        q.position++;
      }
      return q;
    });
    question.position--;
    this.questions = this.questions.sort((a, b) => a.position - b.position);
  }

  moveDown(question: any): void {
    this.questions = this.questions.map(q => {
      if (q.position === question.position + 1) {
        q.position--;
      }
      return q;
    });
    question.position++;
    this.questions = this.questions.sort((a, b) => a.position - b.position);
  }

  changeType(responseType: any, question: any): void {
    question.responseType = responseType;
  }

  close(): void {
    let segmentedUrl = this.utilsService.getUrlByFirstSegments(this.router.url, 4);

    this.router.navigateByUrl(segmentedUrl);
  }
}

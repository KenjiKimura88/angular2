import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { CampaignContact, CampaignContactStatuses } from '../../../../../models/campaign-contact';
import { CampaignContactService } from '../../../../../services/campaign/campaign.contact.service';
import { CallReviewService } from '../../../../../services/campaign/call.review.service';
import { Rating } from '../../../../../models/rating';
import { TranslateService } from '../../../../../services/translate/translate.service';
import { CONTACT_LIST_TABS } from '../contact-list.component';

@Component({
  selector: 'user-information',
  templateUrl: './user-information.component.html',
})
export class UserInformationComponent implements OnInit, OnChanges {

  @Input() selectedContact: CampaignContact;
  @Output() chgOpenTab: EventEmitter<any> = new EventEmitter();
  @Output() updateContact: EventEmitter<any> = new EventEmitter<any>();
  @Output() deleteContact: EventEmitter<any> = new EventEmitter<any>();
  public statuses: any = {
    incompleted: {
      class: 'incompleted',
      label: 'Call Back'
    },
    dnc: {
      class: 'dnc',
      label: 'Do Not Call'
    },
    failed: {
      class: 'failed',
      label: 'No Answer'
    },
    completed: {
      class: 'completed',
      label: 'Completed'
    },
    untried: {
      class: 'untried',
      label: 'Untried'
    },
    voicemail: {
      class: 'voicemail',
      label: 'Voicemail'
    }
  };
  tipsList: number[] = [5, 10, 20];

  tabs: any = CONTACT_LIST_TABS;

  public statusIds: string[] = Object.keys(this.statuses);

  public callReview: Rating;

  constructor(private toastr: ToastsManager,
              private service: CampaignContactService,
              private callReviewService: CallReviewService,
              private translateService: TranslateService) {
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: any): void {
    if (changes.selectedContact) {
      if (this.selectedContact.lastCall) {
        this.callReview = this.selectedContact.lastCall.rating;
        if (!this.callReview && this.selectedContact.triedCall) {
          this.callReview = this.callReviewService.getNewModel();
        }
      } else {
        this.callReview = null;
      }
    }
  }

  setOpenTab(tab: string): void {
    this.chgOpenTab.emit(tab);
  }

  onChangeContactStatus(newStatus: CampaignContactStatuses): void {
    this.selectedContact.status = newStatus;
    let subs = this.service.updateItem(this.selectedContact).subscribe(
      (item: CampaignContact) => {
        this.selectedContact = item;
        this.updateContact.emit(this.selectedContact);
      },
      err => err,
      () => subs.unsubscribe()
    );
  }

  onDeleteContact(): void {
    let subs = this.service.deleteItem(this.selectedContact)
      .subscribe(
        () => {
          this.deleteContact.emit(this.selectedContact);
        },
        err => err,
        () => subs.unsubscribe()
      );
  }

  setRateCall(rateVal: number): void {
    this.callReview.rate = rateVal;
  }

  rateCall(): void {
    this.callReview.callId = this.selectedContact.lastCall.id;
    let subs = this.callReviewService.updateItem(this.callReview).subscribe(
      (item: any) => {
        this.callReview = item;
        this.selectedContact.lastCall.rating = item;
        this.toastr.success(this.translateService.translate('Saved'));
      },
      err => err,
      () => subs.unsubscribe()
    );
  }
}

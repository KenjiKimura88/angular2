import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Campaign } from '../../../../models/campaign';
import { CampaignService } from '../../../../services/campaign/campaign.service';
import { Subscription } from 'rxjs';
import { BudgetExpenseType } from '../../../../models/campaign-budget';

@Component({
  selector   : 'campaign-advanced',
  templateUrl: './campaign-advanced.component.html'
})
export class CampaignAdvancedComponent implements OnInit, OnDestroy {
  campaign: Campaign;
  subs: Subscription;
  budgetExpenseType: any = BudgetExpenseType;

  // TODO, update statuses
  scriptsCompleted: boolean   = false;
  contactsCompleted: boolean  = false;
  callerIdCompleted: boolean  = false;
  callersCompleted: boolean   = true;
  budgetCompleted: boolean    = false;
  timeCompleted: boolean      = true;
  resourcesCompleted: boolean = true;

  // TODO, get this from a service or database
  campaignTips: Array<string> = [ // Referred to as 'help and tips array'
    'Tip 1',
    'Tip 2',
    'Tip 3',
    'Tip 4',
  ];

  constructor(private campaignService: CampaignService,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.campaign = this.route.snapshot.parent.parent.data['campaign'];
    this.refreshCampaign(this.campaign.id);
    this.updateFlags();
  }

  ngOnDestroy(): void {
    if (this.subs) {
      this.subs.unsubscribe();
    }
  }

  refreshCampaign(id: string): void {
    this.subs = this.campaignService.getItem(id)
      .subscribe(campaign => this.campaign = campaign);
  }

  updateFlags(): void {
    this.scriptsCompleted  = !!this.campaign.numberOfQuestions;
    this.contactsCompleted = !!this.campaign.numberOfContacts;
    this.budgetCompleted   = !!(this.campaign.campaignBudget && this.campaign.campaignBudget.id);
    this.callerIdCompleted = !!(this.campaign.callerId && this.campaign.callerId.id);
  }

}

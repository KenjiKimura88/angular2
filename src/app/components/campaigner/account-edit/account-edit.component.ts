import { Component, OnInit } from '@angular/core';
import { ToastsManager } from 'ng2-toastr';
import * as CountryList from 'country-list';
import * as _ from 'lodash';
import { TranslateService } from '../../../services/translate/translate.service';
import { UtilsService } from '../../../services/utils.service';
import { Country } from '../../../models/country';
import { Address } from '../../../models/address';
import { ContactAddressService } from '../../../services/contact/contact-address.service';
import { CompanyService } from '../../../services/company/company.service';
import { Company } from '../../../models/company';
import { User } from '../../../models/user';
import { UserService } from '../../../services/user/user.service';
import { AuthService } from '../../../services/auth/auth.service';


@Component({
  selector   : 'campaigner-account-edit',
  templateUrl: './account-edit.component.html'
})
export class CampaignerAccountEditComponent implements OnInit {
  company: Company;
  user: User;
  countries: Country[];
  address: Address;
  password: any                    = {};
  debouncedUpdateAddress: Function = this.debounce(this.updateAddress);
  debouncedUpdateUser: Function    = this.debounce(this.updateUser);
  errorsStore: any                 = {
    address : {},
    phone   : {},
    password: {},
    user    : {}
  };

  private _countryListService: any;

  debounce(modelFn: Function): Function {
    return _.debounce((...args) => modelFn.apply(this, args), 1000, {trailing: true});
  }

  constructor(private _toastr: ToastsManager,
              private _translateService: TranslateService,
              private _userService: UserService,
              private _authService: AuthService,
              private _addressService: ContactAddressService,
              private _companyService: CompanyService,
              private _utilsService: UtilsService) {
    this._countryListService = new CountryList();
  }

  ngOnInit(): void {
    this.countries = this._countryListService.getData({company: ''});
    this.address   = this._addressService.getNewModel();
    this.user      = this._userService.getNewModel();
    // todo: change when will be received from API
    let subs       = this._companyService.getItem('127').subscribe(
      (item: any) => {
        this.company = item;
        this.address = item.address;
      },
      this._utilsService.extractApiErrors(this.errorsStore.address),
      () => subs.unsubscribe()
    );
    this._authService.waitForCurrentUserData().then(user => {
      this.user = user;
    });
  }

  messageSuccess(msg: string): void {
    this._toastr.success(this._translateService.translate(msg), '', {maxShown: 1});
  }

  updateAddress(): void {
    this._utilsService.emptyObject(this.errorsStore.address);
    // this.address.contactId = this.contact.id;

    let subs = this._addressService.updateItem(this.address).subscribe(
      (item: any) => {
        this.address = item;
        this.messageSuccess('Saved');
      },
      this._utilsService.extractApiErrors(this.errorsStore.address),
      () => subs.unsubscribe()
    );
  }

  updateUser(): void {
    this._utilsService.emptyObject(this.errorsStore.user);

    let subs = this._userService.updateItem(this.user).subscribe(
      (item: any) => {
        this.user = item;
        this.messageSuccess('Saved');
      },
      this._utilsService.extractApiErrors(this.errorsStore.user),
      () => subs.unsubscribe()
    );
  }

  updatePassword(): void {
    this._utilsService.emptyObject(this.errorsStore.password);

    let updatedItem: any = {
      id                   : this.user.id,
//      oldPass              : this.password.oldPass,
      password             : this.password.newPass,
      password_confirmation: this.password.reNewPass
    };
    let subs             = this._userService.updatePassword(updatedItem).subscribe(
      (item: any) => {
        this.password = item;
        this.messageSuccess('Saved');
      },
      this._utilsService.extractApiErrors(this.errorsStore.password),
      () => subs.unsubscribe()
    );
  }
}

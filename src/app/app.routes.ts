import { Routes } from '@angular/router';

import * as Resolvers from './resolvers';
import * as Cmp from './components';
import {
  PermissionGuard
} from './services/auth/auth.service';
import { PERM_TYPE, USER_ROLE } from './models/permission';

export const ROUTES: Routes = [
  {
    path: '',
    resolve: {
      translations: Resolvers.TranslationsResolver
    },
    children: [
      {
        path: '',
        component: Cmp.Home,
        pathMatch: 'full',
        data: {
          isAllowedRole: [USER_ROLE.admin, USER_ROLE.campaigner]
        },
        canActivate: [PermissionGuard]
      },
      {
        path: 'campaigner',
        data: {
          pageName           : 'campaigner-account',
          isAllowedRole      : [USER_ROLE.admin, USER_ROLE.campaigner]
        },
        children: [
          {
            path: 'account',
            component: Cmp.CampaignerAccountEditComponent
          },
          {
            path: 'invoices',
            children: [
              {
                path: '',
                component: Cmp.CampaignerInvoiceHistoryComponent
              }
            ]
          }
        ]
      },
      {
        path: 'campaigns',
        data: {
          pageName: 'campaigns',
          isAllowedPermission: ['Campaign', PERM_TYPE.manage],
          isAllowedRole: [USER_ROLE.admin, USER_ROLE.campaigner]
        },
        children: [
          {
            path: '',
            component: Cmp.MyCampaignsPage
          },
          {
            path: 'create',
            component: Cmp.CampaignCreateComponent,
            data: {
              pageName: 'campaign-create'
            },
          },
          {
            path: ':id',
            data: {
              pageName: 'campaign-detail'
            },
            resolve: {
              campaign: Resolvers.CampaignResolver
            },
            children: [
              {
                path: '',
                component: Cmp.CampaignComponent,
                resolve: {
                  campaign: Resolvers.CampaignResolver
                }
              },
              {
                path: 'edit',
                component: Cmp.CampaignEditComponent,
                data: {
                  pageName: 'campaign-edit'
                },
                resolve: {
                  campaign: Resolvers.CampaignResolver
                },
                children: [
                  {
                    path: 'express',
                    data: {
                      pageName: 'campaign-edit-express'
                    },
                    children: [
                      {
                        path: '',
                        component: Cmp.CampaignExpressComponent
                      },
                      {
                        path: 'scripts',
                        component: Cmp.ScriptsEditComponent,
                        data: {
                          pageName: 'edit-scripts'
                        }
                      },
                      {
                        path: 'contact-list',
                        component: Cmp.ContactListComponent,
                        data: {
                          pageName: 'contact-list'
                        }
                      },
                      {
                        path: 'contacts',
                        children: [
                          {
                            path: 'import',
                            component: Cmp.ContactsImportComponent,
                            data: {
                              pageName: 'contact-import'
                            }
                          },
                          {
                            path: 'from-crm',
                            component: Cmp.ContactsFromCrmComponent
                          }
                        ]
                      },
                      {
                        path: 'caller-id',
                        component: Cmp.CallerIdComponent
                      }
                    ]
                  },
                  {
                    path: 'advanced',
                    data: {
                      pageName: 'campaign-edit-advanced'
                    },
                    children: [
                      {
                        path: '',
                        component: Cmp.CampaignAdvancedComponent
                      },
                      {
                        path: 'scripts',
                        component: Cmp.ScriptsEditComponent,
                        data: {
                          pageName: 'edit-scripts'
                        }
                      },
                      {
                        path: 'contacts',
                        children: [
                          {
                            path: 'import',
                            component: Cmp.ContactsImportComponent
                          },
                          {
                            path: 'from-crm',
                            component: Cmp.ContactsFromCrmComponent
                          }
                        ]
                      },
                      {
                        path: 'contact-list',
                        component: Cmp.ContactListComponent
                      },
                      {
                        path: 'budget',
                        component: Cmp.BudgetComponent
                      },
                      {
                        path: 'caller-id',
                        component: Cmp.CallerIdComponent
                      }
                    ]
                  },
                  {
                    path: 'settings',
                    children: [
                      {
                        path: '',
                        component: Cmp.CampaignSettingsComponent
                      },
                      {
                        path: 'scripts',
                        component: Cmp.ScriptsEditComponent,
                        data: {
                          pageName: 'edit-scripts'
                        }
                      },
                      {
                        path: 'contact-list',
                        component: Cmp.ContactListComponent
                      },
                      {
                        path: 'contacts',
                        children: [
                          {
                            path: 'import',
                            component: Cmp.ContactsImportComponent,
                            data: {
                              pageName: 'contacts-import'
                            }
                          },
                          {
                            path: 'from-crm',
                            component: Cmp.ContactsFromCrmComponent
                          }
                        ]
                      },
                      {
                        path: 'budget',
                        component: Cmp.BudgetComponent
                      },
                      {
                        path: 'caller-id',
                        component: Cmp.CallerIdComponent
                      }
                    ]
                  },
                  {
                    path: 'callers',
                    children: [
                      {
                        path: '',
                        component: Cmp.CampaignCallersComponent,
                        data: {
                          pageName: 'callers'
                        }
                      },
                      {
                        path: ':id',
                        component: Cmp.CampaignCallerComponent,
                        data: {
                          pageName: 'caller-detail'
                        },
                        resolve: {
                          campaignCaller: Resolvers.CampaignCallerResolver
                        }
                      }
                    ]
                  },
                  {
                    path: 'contact-list',
                    children: [
                      {
                        path: '',
                        component: Cmp.ContactListComponent,
                        data: {
                          pageName: 'contacts-list'
                        }
                      },
                      {
                        path: 'contacts',
                        children: [
                          {
                            path: 'import',
                            component: Cmp.ContactsImportComponent,
                            data: {
                              pageName: 'contacts-import'
                            }
                          },
                          {
                            path: 'from-crm',
                            component: Cmp.ContactsFromCrmComponent,
                            data: {
                              pageName: 'contacts-crm'
                            }
                          }
                        ]
                      }
                    ]
                  }
                ]
              },
              {
                path: 'pay',
                data: {
                  pageName: 'campaign-payment'
                },
                resolve: {
                  campaign: Resolvers.CampaignResolver
                },
                component: Cmp.CampaignPaymentComponent
              }
            ]
          }
        ],
        canActivate: [PermissionGuard]
      },
      {
        path: 'callers',
        canActivate: [PermissionGuard],
        data: {
          // isAllowedPermission: ['Campaign', PERM_TYPE.manage],
          isAllowedRole: [USER_ROLE.admin, USER_ROLE.campaigner]
        },
        children: [
          {
            path: ':id',
            component: Cmp.CallerComponent,
            resolve: {
              campaignCaller: Resolvers.CampaignCallerResolver
            }
          }
        ],
      },
      {
        path: 'communication-center',
        canActivate: [PermissionGuard],
        component: Cmp.CommunicationCenterComponent,
        data: {
          // isAllowedPermission: ['Campaign', PERM_TYPE.manage],
          isAllowedRole: [USER_ROLE.admin, USER_ROLE.campaigner, USER_ROLE.caller],
          pageName: 'communication-center'
        }
      },
      {
        path: 'caller/path',
        component: Cmp.MyCampaignsPage,
        canActivate: [PermissionGuard],
        data: {
          isAllowedRole: [USER_ROLE.admin, USER_ROLE.campaigner]
        }
      },
      { path: '404', component: Cmp.NoContent },
    ]
  },

  { path: '**', component: Cmp.NoContent }
];

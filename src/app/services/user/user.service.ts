import { Injectable } from '@angular/core';
import { JSONAPIBase } from '../json-api.base';
import { Datastore } from '../json-api.base';
import { User } from '../../models/user';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
@Injectable()
export class UserService extends JSONAPIBase {
  domainModel: any  = User;
  getUrl: string    = 'auth/edit';
  patchUrl: string  = 'auth';
  postUrl: string   = 'auth';
  deleteUrl: string = 'auth';

  constructor(datastore: Datastore, protected http: Http) {
    super(datastore);
  }

  updatePassword(model: any): Observable<any> {
    return super.updateItemAdvanced(model, 'auth/password');
  }

}

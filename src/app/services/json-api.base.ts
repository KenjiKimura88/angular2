import 'reflect-metadata';
import { Http, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { JsonApiDatastoreConfig, JsonApiDatastore } from 'angular2-jsonapi';
import { APP_CONFIG } from '../environment';
import * as models from '../models';
import * as es6template from 'es6-template-strings';

@JsonApiDatastoreConfig({
  baseUrl: APP_CONFIG.apiBase + '/',
  models: (function (): any {
    let exp = {};

    for (let m in models) {
      if (models.hasOwnProperty(m)) {
        let modelClass = models[m];
        let reflectData = Reflect.getMetadata('JsonApiModelConfig', modelClass);
        if (!reflectData) {
          continue; // not a BaseModel
        }
        let modelType = reflectData.type;
        exp[modelType] = modelClass;
      }
    }
    exp['questionFields'] = models.QuestionField;
    exp['campaignCalls'] = models.CampaignCall;
    exp['call'] = models.CampaignCall;
    exp['campaignSettings'] = models.CampaignSettings;

    return exp;
  }())
})

export class Datastore extends JsonApiDatastore {
  constructor(http: Http) {
    super(http);
  }
}

(function (): void {
  (JsonApiDatastore.prototype as any).queryPaged = function (modelType: any, params: any, headers?: any): any {
    let _this = this;
    let options = this.getOptions(headers);
    let url = this.buildUrl(modelType, params);
    return this.http.get(url, options)
      .map(function (res: any): any {
        let body = res.json();
        let pages: any = {};
        Object.keys(body.links).forEach((name) => {
          pages[name] = new URLSearchParams(decodeURIComponent(body.links[name])).get('page[number]');
        });
        // todo: retrieve more accurately
        let totalItems = params.page && params.page.size ? (pages.last || pages.self) * params.page.size : 0;
        return [_this.extractQueryData(res, modelType), totalItems, pages];
      })
      .catch(function (res: any): any {
        return _this.handleError(res);
      });
  };
}());

export abstract class JSONAPIBase {

  abstract domainModel: any;
  abstract getUrl: string;
  abstract patchUrl: string;
  abstract postUrl: string;
  abstract deleteUrl: string;

  static getPlainData(item: any): any {
    let attributes = Reflect.getMetadata('Attribute', item);
    let ret = {id: item.id};
    for (let i in attributes) {
      if (i.substr(0, 1) !== '_') {
        ret[i] = item[i];
      }
    }
    return ret;
  }

  static updateStorageWith(itemTo: any, itemFrom: any): any {
    let plainData = this.getPlainData(itemFrom);
    delete plainData.id;
    for (let i in plainData) {
      if (true) {
        itemTo[i] = plainData[i];
      }
    }
  }

  constructor(protected datastore: Datastore) {
  }

  getItem(id?: string): Observable<any> {
    let url: string = this.getUrl ? es6template(this.getUrl, {id: id}) : null;
    if (url) {
      Reflect.defineMetadata('JsonApiModelConfig', {type: url}, this.domainModel);
    }
    return this.datastore.findRecord(this.domainModel, id);
  }

  getList(params?: any): Observable<any[]> {
    if (this.getUrl) {
      Reflect.defineMetadata('JsonApiModelConfig', {type: es6template(this.getUrl, params)}, this.domainModel);
    }
    return this.datastore.query(this.domainModel, params);
  }

  getListPaged(params?: any): Observable<any[]> {
    let url: string = this.getUrl ? es6template(this.getUrl, params) : null;
    Reflect.defineMetadata('JsonApiModelConfig', {type: url}, this.domainModel);
    return (this.datastore as any).queryPaged(this.domainModel, params);
  }

  updateItem(item: any): Observable<any> {
    let url: string;
    if (item.id) {
      url = this.patchUrl ? es6template(this.patchUrl, item) : url;
    } else {
      url = this.postUrl ? es6template(this.postUrl, item) : url;
    }
    if (url) {
      Reflect.defineMetadata('JsonApiModelConfig', {type: url}, this.domainModel);
    }
    return this.datastore.createRecord(this.domainModel, item).save();
  }

  updateItemAdvanced(model: any, modelUrl: string): Observable<any> {
    Reflect.defineMetadata('JsonApiModelConfig', {type: modelUrl}, this.domainModel);
    let url      = (this.datastore as any).buildUrl(this.domainModel);
    let id       = model.id;
    delete model.id;
    let body = {
      data: {
        type      : modelUrl,
        id        : id,
        attributes: model
      }
    };
    return this[id ? 'patch' : 'post'](url, body)
      .map((res: any): any => {
        return (this.datastore as any).extractRecordData(res, this.domainModel, model);
      })
      .catch((res: any): any => {
        return (this.datastore as any).handleError(res);
      });
  }

  getNewModel(data?: any): any {
    return this.datastore.createRecord(this.domainModel, data);
  }

  deleteItem(item: any): Observable<any> {
    Reflect.defineMetadata('JsonApiModelConfig', {type: this.deleteUrl}, this.domainModel);
    return this.datastore.deleteRecord(this.domainModel, item.id);
  }
}

import { Injectable } from '@angular/core';
import { JSONAPIBase } from '../json-api.base';
import { Datastore } from '../json-api.base';
import { Campaign } from '../../models/campaign';
import { Observable } from 'rxjs';


@Injectable()
export class CampaignService extends JSONAPIBase {
  domainModel: any  = Campaign;
  // @todo put urls and remove base methods
  getUrl: string    = '';
  patchUrl: string  = '';
  postUrl: string   = '';
  deleteUrl: string = '';

  protected registry: Array<Observable<any>> = [];

  constructor(datastore: Datastore) {
    super(datastore);
  }

  getItem(id: string): Observable<Campaign> {
    if (!this.registry[id]) {
      this.registry[id] = super.getItem(id);
    }
    return this.registry[id];
  }

  // updateItem(item: any): any {
  //   return super.updateItem(item).subscribe(returnedItem => this.refreshCampaign(item));
  // }

  // refreshCampaign(campaign: Campaign): void {
  //   if (campaign.id && this.registry[campaign.id]) {
  //     super.getItem(campaign.id).subscribe(
  //       item => this.registry[campaign.id].next(item)
  //     );
  //   }
  // }
}

import { Injectable } from '@angular/core';
import { JSONAPIBase } from '../json-api.base';
import { Datastore } from '../json-api.base';
import { CampaignContact } from '../../models/campaign-contact';
import { Observable } from 'rxjs/Observable';
import { APP_CONFIG } from '../../environment';
import { Http } from '@angular/http';

@Injectable()
export class CampaignContactService extends JSONAPIBase {
  domainModel: any = CampaignContact;
  // @todo put urls and remove base methods
  getUrl: string = 'campaigns/${campaignId}/contacts';
  patchUrl: string = 'contacts';
  postUrl: string = 'campaigns/${campaignId}/contacts';
  deleteUrl: string = 'contacts';

  constructor(datastore: Datastore, private http: Http) {
    super(datastore);
  }

  getList(params?: any): Observable<any[]> {
    const id = params.id;
    delete params.id;
    let modelClass = `campaigns/${id}/contacts`;
    Reflect.defineMetadata('JsonApiModelConfig', {type: modelClass}, CampaignContact);
    return this.datastore.query(CampaignContact, params);
  }

  deleteItem(item: any): Observable<any> {
    Reflect.defineMetadata('JsonApiModelConfig', {type: 'contacts'}, this.domainModel);
    return this.datastore.deleteRecord(this.domainModel, item.id);
  }

  bulkImport(data: any): Promise<any> {
    let url = APP_CONFIG.apiBase + `/campaigns/${data.campaign_id}/contacts/bulk_upload`;
    return this.http.post(url, data).toPromise()
      .then(
        (response: any) => {
          return Promise.resolve(JSON.parse(response._body));
        }
      )
      .catch(
        (response: any) => {
          return Promise.resolve(JSON.parse(response._body));
        }
      );
  }
}

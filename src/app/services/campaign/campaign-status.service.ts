import { Injectable } from '@angular/core';
import { JSONAPIBase } from '../json-api.base';
import { Datastore } from '../json-api.base';
import { Campaign } from '../../models/campaign';
import { Http } from '@angular/http';
import { APP_CONFIG } from '../../environment';

@Injectable()
export class CampaignStatusService extends JSONAPIBase {
  domainModel: any = Campaign;

  getUrl: string = '';
  patchUrl: string = 'campaigns';
  postUrl: string = '';
  deleteUrl: string = '';

  constructor(datastore: Datastore, private http: Http) {
    super(datastore);
  }

  updateStatus(campaign: Campaign): Promise<any> {
    let url = APP_CONFIG.apiBase + `/campaigns/${campaign.id}/${campaign.status}`;
    let campaignId = this.getNewModel({});
    return this.http.patch(url, '').toPromise()
      .then((result: any): any => {
        return (this.datastore as any).extractRecordData(result, this.domainModel, campaignId);
      })
      .catch((result: any): any => {
        return (this.datastore as any).handleError(result);
      });
  }
}

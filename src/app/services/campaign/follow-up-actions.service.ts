import { Injectable } from '@angular/core';
import { JSONAPIBase } from '../json-api.base';
import { Datastore } from '../json-api.base';
import { FollowUpActions } from '../../models/follow-up-actions';
import { Observable } from 'rxjs/Observable';
import { BaseModel } from '../../base.model';

@Injectable()
export class FollowUpActionsService extends JSONAPIBase {
  domainModel: any = FollowUpActions;
  // @todo put urls and remove base methods
  getUrl: string = '';
  patchUrl: string = '';
  postUrl: string = '';
  deleteUrl: string = '';

  constructor(datastore: Datastore) {
    super(datastore);
  }
  updateChildItem(item: any, campaignId: string): Observable<BaseModel> {
    let modelClass = `campaigns/${campaignId}/follow_up_actions`;

    if (item.id) {
      modelClass = 'follow_up_actions';
    }

    Reflect.defineMetadata('JsonApiModelConfig', {type: modelClass}, FollowUpActions);

    let newModel = this.datastore.createRecord(FollowUpActions, item);

    return newModel.save();
  }
  getItem(id?: string): Observable<any> {
    let modelClass = 'follow_up_actions';

    Reflect.defineMetadata('JsonApiModelConfig', {type: modelClass}, FollowUpActions);

    return this.datastore.findRecord(FollowUpActions, id);
  }
}

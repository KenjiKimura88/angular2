import { Injectable } from '@angular/core';
import { JSONAPIBase } from '../json-api.base';
import { Datastore } from '../json-api.base';
import { QuestionField } from '../../models/question.field';

import { Observable } from 'rxjs/Observable';

@Injectable()
export class QuestionFieldsService extends JSONAPIBase {
  domainModel: any = QuestionField;
// @todo put urls and remove base methods
  getUrl: string = '';
  patchUrl: string = '';
  postUrl: string = '';
  deleteUrl: string = '';

  constructor(datastore: Datastore) {
    super(datastore);
  }
  getList(params?: any): Observable<any[]> { // return the model with which we work
    const id = params.id;
    delete params.id;
    let modelClass = `questions/${id}/question_fields`;

    Reflect.defineMetadata('JsonApiModelConfig', {type: modelClass}, QuestionField);

    return this.datastore.query(QuestionField, params);
  }
  updateChildItem(item: any, campaignId: string): Observable<any> {
    let modelClass = (item.id) ? 'question_fields' : `questions/${campaignId}/question_fields`;

    Reflect.defineMetadata('JsonApiModelConfig', {type: modelClass}, QuestionField);

    let newModel = this.datastore.createRecord(QuestionField, item);

    return newModel.save();
  }
  removeChildItem(item: any, campaignId: string): Observable<any> {
    let modelClass = `question_fields`;

    Reflect.defineMetadata('JsonApiModelConfig', {type: modelClass}, QuestionField);

    return this.datastore.deleteRecord(QuestionField, item.id);
  }
}

import { Injectable } from '@angular/core';
import { JSONAPIBase } from '../json-api.base';
import { Datastore } from '../json-api.base';
import { Question } from '../../models/question';

import { Observable } from 'rxjs/Observable';
import { BaseModel } from '../../base.model';

@Injectable()
export class QuestionService extends JSONAPIBase {
  domainModel: any = Question;
// @todo put urls and remove base methods
  getUrl: string = '';
  patchUrl: string = '';
  postUrl: string = '';
  deleteUrl: string = '';

  constructor(datastore: Datastore) {
    super(datastore);
  }

  getList(params?: any): Observable<any[]> { // return the model with which we work
    const id = params.id;
    delete params.id;
    let modelClass = `campaigns/${id}/questions`;

    Reflect.defineMetadata('JsonApiModelConfig', {type: modelClass}, Question);

    return this.datastore.query(Question, params);
  }

  updateChildItem(item: any, campaignId: string): Observable<BaseModel> {
    let modelClass = `campaigns/${campaignId}/questions`;

    if (item.id) {
      modelClass = 'questions';
    }

    Reflect.defineMetadata('JsonApiModelConfig', {type: modelClass}, Question);

    let newModel = this.datastore.createRecord(Question, item);

    return newModel.save();
  }

  removeChildItem(item: any, campaignId: string): Observable<any> {
    let modelClass = `questions`;

    Reflect.defineMetadata('JsonApiModelConfig', {type: modelClass}, Question);

    return this.datastore.deleteRecord(Question, item.id);
  }
}

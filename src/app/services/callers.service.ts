import { Injectable } from '@angular/core';
import { JSONAPIBase } from './json-api.base';
import { Datastore } from './json-api.base';
import { Caller } from '../models/caller';

@Injectable()
export class CallersService extends JSONAPIBase {
  domainModel: any = Caller;
  getUrl: string = 'callers';
  patchUrl: string = '';
  postUrl: string = '';
  deleteUrl: string = '';

  constructor(datastore: Datastore) {
    super(datastore);
  }
}

import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Injectable()
export class UtilsService {
  constructor(private _route: ActivatedRoute,
              private _router: Router) {
  }

  replaceArrayVal(arr: Array<any>, newVal: Array<any>): void {
    arr.splice.apply(arr, [0, arr.length].concat(newVal));
  }

  emptyObject(obj: any): void {
    Object.keys(obj).forEach((key) => delete obj[key]);
  }

  // todo: refactor & move in to magic form validator helper directive
  extractApiErrors(errStore: any): any {
    return (e: any): any => {
      this.emptyObject(errStore);
      for (let i in e.errors) {
        if (e.errors.hasOwnProperty(i)) {
          let fieldName       = e.errors[i].source.pointer.split('/').pop();
          errStore[fieldName] = e.errors[i].detail;
        }
      }
    };
  }

  getCloserRouteData(dataKey: string): any {
    let dataHierarchy  = [];
    let currentRoute   = this._route.root;
    let hasChildRoutes = false;
    do {
      let childrenRoutes = currentRoute.children;
      hasChildRoutes     = false;
      childrenRoutes.forEach(route => {
        if (route.outlet === 'primary') {
          if (route.snapshot && route.snapshot.data) {
            dataHierarchy.push(route.snapshot.data[dataKey]);
          }
          currentRoute   = route;
          hasChildRoutes = true;
        }
      });
    } while (hasChildRoutes);
    return dataHierarchy.pop();
  }

  getUrlByFirstSegments(url: string, nrSegments: number): string {
    let urlTree         = this._router.parseUrl(url);
    let urlSegmentGroup = (urlTree.root.children as any).primary;
    let segmentedUrl    = '';

    for (let nr = 0; nr < nrSegments; nr++) {
      segmentedUrl += `/${urlSegmentGroup.segments[nr]}`;
    }

    return segmentedUrl;
  }
}

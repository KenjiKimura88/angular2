import { Injectable } from '@angular/core';
import { JSONAPIBase } from '../json-api.base';
import { Datastore } from '../json-api.base';
import { Company } from '../../models/company';

@Injectable()
export class CompanyService extends JSONAPIBase {
  domainModel: any = Company;
  getUrl: string = 'companies';
  patchUrl: string = 'companies';
  postUrl: string = 'companies';
  deleteUrl: string = 'companies';

  constructor(datastore: Datastore) {
    super(datastore);
  }
}
